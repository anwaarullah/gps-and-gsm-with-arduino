This project example will walk through the process of integrating GSM and GPS modules with an Arduino. This project detects for a signal from a tilt and pressure sensor and then sends out an alert SMS with the GPS co-ordinates. This can be used in accident alerts, vehicle tracking, kids monitoring and a host of other applications.

The following Libraries are required for the Sketch:

1. TinyGPS (attached in repo)
2. SerialGSM - [Download](https://github.com/meirm/SerialGSM)

Also tested with uBlox-Neo-6M GPS Module.

[Project Video Demo](https://www.youtube.com/watch?v=8L4T383APeU)