#include <TinyGPS.h>
#include <LiquidCrystal.h>
#include <SerialGSM.h>
#include <SoftwareSerial.h>

char latitude[15];
char longitude[15];

char lat[12] = "Latitude: " ;
char lon[14] = " Longitude: ";

char message[160];

SerialGSM cell(2,3);
int tiltSensorPin = 8;
int pressurePin = 9;

TinyGPS gps;

static void smartdelay(unsigned long ms);
static void print_float(float val, float invalid, int len, int prec);
static void print_int(unsigned long val, unsigned long invalid, int len);
static void print_str(const char *str, int len);
LiquidCrystal lcd(12, 11, 7, 6, 5, 4);
int tiltState = 0;    

void setup()
{
  Serial.begin(9600);
  cell.begin(9600);
  cell.Verbose(true);
  cell.Boot(); 
  cell.FwdSMS2Serial();
  pinMode(tiltSensorPin, INPUT); 
  digitalWrite(tiltSensorPin, HIGH);
  pinMode(pressurePin, INPUT); 
  digitalWrite(pressurePin, HIGH);

  lcd.begin(16, 2);
  Serial.println("Testing GPS");
  Serial.println( "Latitude Longitude");
  Serial.println("----------------------");

  // Show Latitude and Longitude Values on LCD
  delay(1000);
  lcd.setCursor(0,0);
  //sendSMS(); 

  cell.Rcpt("+919908877764");
  cell.Message(message);
  cell.SendSMS();
}

float flat, flon;
unsigned long age, date, time, chars = 0;
unsigned short sentences = 0, failed = 0;

boolean flag = true;

int m = 0;

void loop()
{
  gps.f_get_position(&flat, &flon, &age);
  //  print_float(flat, TinyGPS::GPS_INVALID_F_ANGLE, 10, 6);
  //  print_float(flon, TinyGPS::GPS_INVALID_F_ANGLE, 11, 6);

  dtostrf(flat,10, 7, latitude);
  dtostrf(flon,10, 7, longitude);  

  //Serial.println(latitude);
  //Serial.println(longitude);

  memset(message, '\0', 160);

  strcat(message, lat);
  strcat(message, latitude);
  strcat(message, lon);
  strcat(message, longitude);
  Serial.println(message); 

  //lcd.clear();
  delay(1000);
  lcd.setCursor(0,0);
  lcd.print( "Lat:");
  lcd.print(flat, DEC);
  lcd.setCursor(0,1);
  lcd.print( "Long:");
  lcd.print(flon, DEC);

  if (((digitalRead(tiltSensorPin) == LOW) || (digitalRead(pressurePin) == LOW)) && (flag == true))  {
    digitalWrite(tiltSensorPin, HIGH);     
    digitalWrite(pressurePin, HIGH);     
    flag = false;
    sendSMS();   
  }  

  //  Serial.println(dtostrf(flat,8,0,0));
  smartdelay(1000);
}



static void smartdelay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (Serial.available())
      gps.encode(Serial.read());
  } 
  while (millis() - start < ms);
}

static void print_float(float val, float invalid, int len, int prec)
{
  if (val == invalid)
  {
    while (len-- > 1)
      Serial.print('*');
    Serial.print(' ');
  }
  else
  {
    Serial.print(val, prec);
    int vi = abs((int)val);
    int flen = prec + (val < 0.0 ? 2 : 1); // . and -
    flen += vi >= 1000 ? 4 : vi >= 100 ? 3 : vi >= 10 ? 2 : 1;
    for (int i=flen; i<len; ++i)
      Serial.print(' ');
  }
  smartdelay(0);
}

static void print_int(unsigned long val, unsigned long invalid, int len)
{
  char sz[32];
  if (val == invalid)
    strcpy(sz, "*******");
  else
    sprintf(sz, "%ld", val);
  sz[len] = 0;
  for (int i=strlen(sz); i<len; ++i)
    sz[i] = ' ';
  if (len > 0) 
    sz[len-1] = ' ';
  Serial.print(sz);
  smartdelay(0);
}

static void print_str(const char *str, int len)
{
  int slen = strlen(str);
  for (int i=0; i<len; ++i)
    Serial.print(i<slen ? str[i] : ' ');
  smartdelay(0);
}

//String flatS = flat;
//String flonS = flon;

void sendSMS()
{
  cell.Rcpt("+919908877764");
  cell.Message(message);
  cell.SendSMS();
  flag = true;
}

